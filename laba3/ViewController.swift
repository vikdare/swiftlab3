//
//  ViewController.swift
//  laba3
//
//  Created by Admin on 02.03.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SWXMLHash

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var value: Double = 0
    var valutes:[MyValute] = [MyValute(name: "", value: "")]
    
    @IBAction func Keyboard(sender: AnyObject) {
        view.endEditing(true)
    }
    
    @IBAction func Convert(sender: AnyObject) {
        if let val = Double(textField.text!) {
            if val >= 0 {
                self.value = val
            }
        }
        self.valutes.removeAll()
        Alamofire.request(.GET, "https://www.nbrb.by/Services/XmlExRates.aspx", parameters: nil).response {
            (req, res, dat, err) in
            if dat != nil {
                let xml = SWXMLHash.parse(dat!)
                for index in xml["DailyExRates"]["Currency"] {
                    let name = index["Name"].element!.text!
                    let rate = index["Rate"].element!.text!
                    let val = MyValute(name: name, value: rate)
                    self.valutes.append(val)
                    //self.tableView.reloadData()
                }
            }
            self.tableView.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.valutes.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("tableCell")
        cell?.textLabel?.text = valutes[indexPath.row].name
        cell?.detailTextLabel?.text = String(format:"%.4f", self.value/self.valutes[indexPath.row].value)
        return cell!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

