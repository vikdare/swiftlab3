//
//  MyValute.swift
//  laba3
//
//  Created by Admin on 02.03.16.
//  Copyright © 2016 Admin. All rights reserved.
//

class MyValute {
    var name = ""
    var value = 0.0
    
    init (name: String, value: String) {
        self.name = name
        if let val = Double(value) {
            self.value = val
        }
    }
}
